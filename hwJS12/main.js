const showImage = () => {
	let imageList = document.querySelectorAll('img')
	let activeItem = ""
	imageList.forEach((elem) => {
		if (elem.classList.contains('active')) {
			activeItem = elem
		}
	})
	let needActivate = activeItem.nextSibling
	if (needActivate.data === "\"\\r\"\"\\n\"") {
		needActivate = activeItem.nextSibling
	} else if (needActivate.data === "\n") {
		needActivate = imageList[0]
	}
	needActivate.classList.add('active')
	activeItem.classList.remove('active')
}
let intervalId = setInterval(showImage, 3000)

let stop = document.querySelector('#stop')
stop.addEventListener('click', () => {
	clearInterval(intervalId)
})
let start = document.querySelector('#start')
start.addEventListener('click', () => {
	intervalId = setInterval(showImage, 3000)
})
