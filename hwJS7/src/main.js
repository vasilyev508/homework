//Опишите своими словами, как Вы понимаете, что такое Document Object Model (DOM)

//DOM -це модель відображення документу через певну ієрархічну структуру з розгалудженнями.
//за допомогою такої моделі можливо "ходити" по такому дереву від дітей до батьків та навпаки, а також до сусідніх елементів.

//Реализовать функцию, которая будет получать массив элементов и выводить их на страницу в виде списка. Задача должна быть реализована на языке javascript, без использования фреймворков и сторонних библиотек (типа Jquery).

//Технические требования:
//Создать функцию, которая будет принимать на вход массив и опциональный второй аргумент parent - DOM-элемент, к которому будет прикреплен список (по дефолту должен быть document.body).
//Каждый из элементов массива вывести на страницу в виде пункта списка;
//Используйте шаблонные строки и метод map массива для формирования контента списка перед выведением его на страницу;
//Примеры массивов, которые можно выводить на экран:
//	["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
//["1", "2", "3", "sea", "user", 23];
//Можно взять любой другой массив.

function renderData(arr = [], domElemQuery ='body') {
	let listElem = document.createElement("ul");
	let domElem = document.querySelector(domElemQuery)
	arr.forEach((elem) => {
		let item = document.createElement('li')
		item.innerText = elem
		listElem.appendChild(item)
	})
	domElem.appendChild(listElem)
}

renderData(["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"])
renderData(["1", "2", "3", "sea", "user", 23])

//Необязательные задания продвинутой сложности:


//	Добавьте обработку вложенных массивов. Если внутри массива одним из элементов будет еще один массив, выводить его как вложенный список.
//	Пример такого массива:

//	["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"];
function renderDataRecursion(arr = [], domQuerySelector = 'body') {
	let counterOfDeep = 0;
	let listElem = document.createElement("ul");
	let domElem = document.querySelector(domQuerySelector)
	domElem.appendChild(listElem)
	arr.forEach((elem, i) => {
		if (Array.isArray(elem)) {
			let tmpElem = document.createElement('li')
			tmpElem.classList.add(`list${Math.floor(Math.random() * 10000000)}`)
			listElem.appendChild(tmpElem)
			renderDataRecursion(elem, `.${tmpElem.classList}`)
		} else {
			let node = document.createElement('li');
			node.innerText = elem;
			listElem.appendChild(node)
		}})
}

renderDataRecursion(["Kharkiv", "Kiev", ["Borispol", "Irpin",["Borispol", "Irpin"]], "Odessa", "Lviv", "Dnieper"])

//Подсказка: используйте map для обхода массива и рекурсию, чтоб обработать вложенные массивы.

//Очистить страницу через 3 секунды. Показывать таймер обратного отсчета (только секунды) перед очищением страницы.
const clear = () => {
	document.querySelector('body').innerHTML = ''
}
setTimeout(clear, 3000)

