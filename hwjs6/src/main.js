function filterBy(array=[], filterType) {
	return array.filter((elem) => typeof elem !== filterType)
}

console.log(filterBy( ['hello', 'world', 23, '23', null], 'string'))

// цыкл forEach даёт возможность выполнить функцию над каждым элементом масива
