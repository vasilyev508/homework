const colloredBtn = (buttonValue) => {
	let buttons = document.querySelectorAll('.btn')
	buttons.forEach((el) => {
		if (el.classList.contains('collored')) {
			el.classList.remove('collored')
		}
		if (el.innerText === buttonValue) {
			el.classList.add('collored')
		}
	})
}
document.addEventListener('keydown', (ev => {
	console.log(ev.key)
	let key = ev.key
	if (key.length === 1) {
		colloredBtn(key.toUpperCase())
	} else {
		colloredBtn(key)
	}
}))
