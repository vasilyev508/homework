/*створюэмо глобальні змінні з якими далі будемо працювати*/
let requestedNumber1, requestedNumber2, operator;

/*отримуємо від користувача інформацію*/
do {
	requestedNumber1 = prompt('please enter first number', 0)
}
while (!isValidNumber(requestedNumber1))
console.log(`first number == ${requestedNumber1}`)

do {
	requestedNumber2 = prompt('please enter second number', 0)
}
while (!isValidNumber(requestedNumber2))
console.log(`second number == ${requestedNumber2}`)

do {
	operator = prompt('please math operator "+" or "-" or "/" or "*"', '+')
}
while (!isMathOperator(operator))
console.log('operator = ' + operator)

/*виконуємо розрахунки*/
result = makeMathOperation(parseInt(requestedNumber1), parseInt(requestedNumber2), operator);

/*виводимо результат*/
console.log(`${requestedNumber1} ${operator} ${requestedNumber2} == ${result}`)


/*оголошуємо функції*/
function isValidNumber(number) {
	if (number === 'Infinity') {
		return false;
	}
	if ((number > 0) || (number < 0) || (number === "0")) {
		return true;
	}
	return false
}

function isMathOperator(operator) {
	switch (operator) {
		case '-' :
			return true;
		case '+' :
			return true;
		case '/' :
			return true;
		case '*' :
			return true;
		default:
			return false
	}
}

function makeMathOperation(num1, num2, operator) {
	switch (operator) {
		case '-':
			return num1 - num2;
		case '+':
			return num1 + num2;
		case '/':
			if (num2 === 0) {
				console.log('ZeroDivisionError: division by zero')
				return false
			}
			return num1 / num2;
		case '*':
			return num1 * num2;
		default:
			return false
	}
}

/*
Описать своими словами для чего вообще нужны функции в программировании.

функція необхідні для виконання частини коду декілька разів можливо з іншими параметрами, можливо в різних місцях коду
та допомогає підтримувати код шляхом DRY принципу програмування


Описать своими словами, зачем в функцию передавать аргумент.
передавати аргументи необзідно для того щоб в зими щось робити в тілі функції, це можуть бути як математичні так і інші
операції

*/
