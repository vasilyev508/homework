//Опишите своими словами, как Вы понимаете, что такое обработчик событий.
//опрацьовувач подій це певний метод який есть у ДОМ елемента з переліком перних подій
//його задача полягає у тому чи стпрацювала у певного елементе подія

/*Создать поле для ввода цены с валидацией. Задача должна быть реализована на языке javascript, без использования
фреймворков и сторонник библиотек (типа Jquery).

Технические требования:

	При загрузке страницы показать пользователю поле ввода (input) с надписью Price. Это поле будет служить для ввода
	числовых значений
Поведение поля должно быть следующим:

	При фокусе на поле ввода - у него должна появиться рамка зеленого цвета. При потере фокуса она пропадает.
	Когда убран фокус с поля - его значение считывается, над полем создается span, в котором должен быть выведен текст:
	Текущая цена: ${значение из поля ввода}. Рядом с ним должна быть кнопка с крестиком (X).
	Значение внутри поля ввода окрашивается в зеленый цвет.
	При нажатии на Х - span с текстом и кнопка X должны быть удалены. Значение, введенное в поле ввода, обнуляется.
	Если пользователь ввел число меньше 0 - при потере фокуса
	подсвечивать поле ввода красной рамкой, под полем выводить фразу -
	Please enter correct price. span со значением при этом не создается.

	В папке img лежат примеры реализации поля ввода и создающегося span.*/
const priceField = document.getElementById('priceField');

const emptyPriceField = () => {
	priceField.value = '';
	priceField.style.border = ('1px solid black');
	priceField.style.color = ('black');
};

const correctPriceAnswer = (price) => {

	let div = document.getElementById("correctPriceAnswer");
	let span = document.createElement("span");
	priceField.style.color = ('limegreen');

	if (div.innerText === '') {

		document.body.insertBefore(div, priceField);
		div.appendChild(span);
		span.innerText = `Price is $ ${Number(price)}`;
		span.style.marginRight = (`10px`);

		let button = document.createElement("button");
		div.appendChild(button);
		button.innerText = (`X`);
		button.id = ('buttonX');
	} else {
		div.innerText = '';
		correctPriceAnswer(price);
	}

	document.getElementById('buttonX').addEventListener("click", () => {
		div.innerText = '';
		emptyPriceField();
	});
};

const wrongPriceAnswer = () => {

	let span = document.createElement('span');

	span.innerText = (`Please enter correct price`);
	span.style.display = 'block';
	document.body.insertBefore(span, priceField.firstChild);
	priceField.style.border = (`2px solid red`);
	priceField.style.color = ('black');

	priceField.addEventListener("focus", () => {
		span.remove();
		priceField.style.border = (`2px solid limegreen`)
	});

	if (document.querySelector('#correctPriceAnswer') !== null) {
		let div = document.querySelector('#correctPriceAnswer')
		div.innerHTML = ""
	}
};

const priceAnswer = (price) => {
	price = Number(priceField.value);
	if (price > 0) {
		correctPriceAnswer(price);
	} else if (price == '') {
		emptyPriceField ();
	} else {
		wrongPriceAnswer();
	}
};

priceField.addEventListener("focus", () => priceField.style.border = ('2px solid limegreen'));
priceField.addEventListener("blur", priceAnswer);


