/*Реализовать переключение вкладок (табы) на чистом Javascript.

Технические требования:

В папке tabs лежит разметка для вкладок. Нужно, чтобы по нажатию на вкладку отображался конкретный текст для нужной
вкладки. При этом остальной текст должен быть скрыт. В комментариях указано, какой текст должен отображаться для какой
вкладки.
Разметку можно менять, добавлять нужные классы, id, атрибуты, теги.
Нужно предусмотреть, что текст на вкладках может меняться, и что вкладки могут добавляться и удаляться. При этом нужно,
 чтобы функция, написанная в джаваскрипте, из-за таких правок не переставала работать.*/

let tabButtonList = document.querySelectorAll('.tabs-title')
tabButtonList.forEach((elem) => {
	elem.addEventListener('click', (e) => {
		let value = e.currentTarget.innerText.toLowerCase()
		let arrOfInfo = document.querySelectorAll('[data-tabs-description]')
		arrOfInfo.forEach((elem) => {
			elem.classList.remove('active')
			document.querySelector(`[data-tabs-description="${value}"]`).classList.add('active')
		})
		tabButtonList.forEach((elemOfArr) => {
			elemOfArr.classList.remove('active')
		})
		elem.classList.add('active')
	})
})

//далі то я так....грався, врахозувати його не обов'язково

document.querySelector('.add').addEventListener('click', () => {
	const newTitle = getNewTitle()
	const newDescription = getDescription()
	const itemName = document.createElement('li')
	itemName.classList.add('tabs-title')
	itemName.textContent = newTitle[0].toUpperCase() + newTitle.slice(1);
	document.querySelector('ul').appendChild(itemName)
	const itemDescription = document.createElement('li')
	itemDescription.innerText = newDescription
	itemDescription.dataset.tabsDescription = newTitle.toLowerCase()
	document.querySelector('.tabs-content').appendChild(itemDescription)
	itemName.addEventListener('click', (e) => {
		let value = e.currentTarget.innerText.toLowerCase()
		document.querySelectorAll('.tabs-title').forEach((e) => e.classList.remove('active'))
		e.currentTarget.classList.add('active')
		document.querySelectorAll(`[data-tabs-description]`).forEach((e) => e.classList.remove('active'))
		document.querySelector(`[data-tabs-description="${value}"]`).classList.add('active')
		tabButtonList = document.querySelectorAll('.tabs-title')

	})
})

let getNewTitle = () => {
	const regexLetter = /\d/
	let title = ""
	do {
		title = prompt('enter name').trim()
	} while (regexLetter.test(title))
	return title
}
let getDescription = () => prompt('enter description')
