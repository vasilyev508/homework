function createNewUser() {
	/*Написать функцию createNewUser(), которая будет создавать и возвращать объект newUser.*/

	/*При вызове функция должна спросить у вызывающего имя и фамилию.*/
	let names = gatNames()
	let userBirthday = getBirthday()


	const newUser = Object.create({}, {
			fistName: {
				value: names["firstName"],
			},
			lastName: {
				value: names["secondName"],
			},
			birthday: {
				value: userBirthday,
			},
			Login: {
				get() {
					return `${this.fistName[0] + this.lastName}`.toLowerCase()
				}
			},
			age: {
				get() {
					let today = new Date();
					let year = today.getFullYear()
					return year - this.birthday.getFullYear()
				}
			},
		password: {
				get() {
					return `${this.fistName[0].toUpperCase()}${this.lastName.toLowerCase()}${this.birthday.getFullYear()}`
				}
		},
		},
	)
	return newUser;
}


function gatNames() {
	/*функция должна спросить у вызывающего имя и фамилию.*/
	let firstName = prompt('enter first name')

	while (Number.isInteger(+firstName) || Number.isFinite(+firstName)) {
		firstName = prompt('wrong value for first name')
	}


	let secondName = prompt('enter second name')
	while (Number.isInteger(+secondName) || Number.isFinite(+secondName)) {
		secondName = prompt('wrong value for second name')
	}
	return {firstName, secondName}
}

function getBirthday(date) {
	let userDate = '0';
	let getDateArr = '';
	let birthday = '';
	do {
		userDate = prompt('enter your birthday like "dd.mm.yyyy"');
		if (userDate !== null){
		getDateArr = userDate.split('.')
		console.log(getDateArr.length)
		birthday = new Date(`${getDateArr[2]}-${getDateArr[1]}-${getDateArr[0]}`)}

	} while (isNaN(birthday) || getDateArr.length < 3);
	return birthday
}

const user1 = createNewUser()

console.log(`User == ${user1}`)
console.log(`User age == ${user1.age}`)
console.log(`User password == "${user1.password}"`)


/*Опишите своими словами, что такое экранирование, и зачем оно нужно в языках программирования*/
/*
* в js есть специальные символы на которые навязан специальный функционал.
* одним из спецсимволов есть "\" симсвол екранирования
* он даёт возможность использовать следующий символ который идёт после "\" как простой символ
* к примеру "\"" == строке с простой кавычкой "
* без символа екранирования у нас бы была простая пустая строка
* так же в регулярных выражениях есть спец символы обозначающие число или символ или слово
* чтоб ограничить действие спецсимолов и задавать их как простые символы можно тоже использовать екранирование "\d"
* */
